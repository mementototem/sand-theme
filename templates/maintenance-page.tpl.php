﻿<?php 
/**
 * @file
 * Sand's theme for display single Drupal page when offline.
 *
 * All of the available variables are mirrored in page.html.twig.
 *
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
 ?>
<!DOCTYPE html>
<html<?php print $html_attributes; ?>>
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
  
  <a href="#main-content" class="element-invisible element-focusable skip-link">
    <?php print t('Skip to main content'); ?>
  </a>
  
  <div id="page-wrapper"><div id="page">
    <div id="header-wrapper"><header id="header" role="banner">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
          <img id="site-logo" src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>
      
      <?php if ($site_name || $site_slogan): ?>
        <div id="name-and-slogan"<?php if ($hide_site_name && $hide_site_slogan) { print ' class="element-invisible"'; ?>>
          <?php if ($site_name): ?>
            <div id="site-name"<?php if ($hide_site_name) { print ' class="visually-hidden"'; ?>>
              <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
            </div>
          <?php endif; ?>
          
          <?php if ($site_slogan): ?>
            <div id="site-slogan"<?php if ($hide_site_slogan) { print ' class="visually-hidden"'; ?>>
              <?php print $site_slogan; ?>
            </div>
          <?Php endif; ?>
        </div>
      <?php endif; ?>
    </header></div>
    
    <div id="main-wrapper"><main id="main-content" role="main">
      <section>
        <?php if ($title): ?>
          <h1 class="title" id="page-title"><?php print $title; ?></h1>
        <?php endif; ?>
        
        <?php print $content; ?>
        
        <?php if ($messages): ?>
          <div id="messages">
            <?php print $messages; ?>
          </div>
        <?php endif; ?>
      </section>
    </main></div>
  </div></div>
</body>
</html>
