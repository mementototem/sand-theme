﻿<?php
/**
 * @file
 * Sand's theme for display single Drupal page.
 *
 * All of the available variables are mirrored in bartik's page.html.twig.
 *
 * html, head, body tag are located in core/modules/system/html.html.twig
 * and include by default.
 *
 * @see template_preprocess_page()
 * @see sand_preprocess_page()
 * @see html.html.twig
 *
 * @ingroup themeable
 */
 ?>
<a href="#main-content" class="element-invisible element-focusable skip-link">
  <?php print t('Skip to main content'); ?>
</a>

<div id="page-wrapper"><div id="page">

<div id="header-wrapper"><header id="header" role="banner">
  <?php if ($site_name || $site_slogan || $logo): ?>
    <div id="logo-and-name">
      <?php if ($logo): ?>
        <a href="<?php print $front_page ?>" title="<?php t('Home'); ?>" rel="home">
          <img id="site-logo" src="<?php print $logo; ?>" alt="<?php t('Home'); ?>" />
        </a>
      <?php endif; ?>
      
      <?php if ($site_name || $site_slogan): ?>
        <div id="name-and-slogan"<?php if ($hide_site_name && $hide_site_slogan) { print ' class="visually-hidden"'; } ?>>
          <?php if ($site_name): ?>
            <div id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
              <a href="<?php print $front_page; ?>" title="<?php t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
            </div>
          <?php endif; ?>
          
          <?php if ($site_slogan): ?>
            <div id="site-slogan"<?php if ($hide_site_slogan) { print ' class="element-invisible"'; } ?>>
              <?php print $site_slogan; ?>
            </div>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <?php if ($main_menu): ?>
    <nav id="main-menu" class="navigation" role="navigation">
      <?php print theme('links__system_main_menu', array(
        'links'      => $main_menu,
        'attributes' => array(
          'id'    => 'main-menu-links',
          'class' => array('links', 'clearfix'),
        ),
        'heading'    => array(
          'text'  => t('Main menu'),
          'level' => 'h2',
          'class' => array('element-invisible'),
        ),
      )); ?>
    </nav>
  <?php endif; ?>
</header></div>

<div id="main-wrapper">
  <?php if ($page['header']): ?>
    <div id="block-header">
      <?php print render($page['header']); ?>
    </div>
  <?php endif; ?>
  
  <?php if ($messages): ?>
    <div id="messages">
      <?php print $messages; ?>
    </div>
  <?php endif; ?>
  
  <div id="main" class="clearfloat clearfix">
    <main id="main-content" role="main">
      <section>
        <?php if ($page['featured']): ?>
          <div id="featured">
           <?php print render($page['featured']); ?>
          </div>
        <?php endif; ?>
        
        <?php print render($title_prefix); ?>
          <?php if ($title): ?>
            <h1 class="title" id="page-title"><?php print $title; ?></h1>
          <?php endif; ?>
        <?php render($title_suffix); ?>

        <?php if ($tabs): ?>
          <nav class="tabs" role="navigation"><?php print render($tabs); ?></nav>
        <?php endif; ?>

        <?php print render($page['help']); ?>

        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>

        <?php print render($page['content']); ?>
      </section>
    </main>
    
    <?php if ($page['sidebar']): ?>
      <div id="sidebar-wrapper"><aside>
        <?php print render($page['sidebar']); ?>
      </aside></div>
    <?php endif; ?>
  </div>
</div>

<?php if ($page['footer'] || $page['panel_alpha'] || $page['panel_beta'] || $page['panel_gamma']): ?>
  <div id="footer-wrapper"><footer>
    <?php if ($page['panel_alpha'] || $page['panel_beta'] || $page['panel_gamma']): ?>
    <div id="panel-wrapper" class="n-panel-3 clearfix"><!-- TODO: add panel count for manipulate layout -->
      <div id="panel-alpha" class="panel">
        <?php print render($page['panel_alpha']); ?>
      </div>
      <div id="panel-beta" class="panel">
        <?php print render($page['panel_beta']); ?>
      </div>
      <div id="panel-gamma" class="panel">
        <?php print render($page['panel_gamma']); ?>
      </div>
    </div>
    <?php endif; ?>
    
    <?php if ($page['footer']): ?>
    <div id="footer">
      <?php print render($page['footer']); ?>
    </div>
    <?php endif; ?>
  </footer></div>
<?php endif; ?>

</div></div><!-- #page, #page-wrapper -->
