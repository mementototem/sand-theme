﻿<?php
/**
 * @file
 * Sand's theme implementation to display a node.
 *
 * All of the available variables are mirrored in bartik's page.html.twig.
 *
 * @see template_preprocess_node()
 *
 * @ingroup themeable
 */
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix" role="article" <?php print $attributes; ?>>

  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page): ?>
      <h2 class="node-title"<?php print $title_attributes; ?>>
        <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
      </h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>

    <?php if ($display_submitted): ?>
      <div class="meta submitted">
        <?php print $submitted; ?>
      </div>
    <?php endif; ?>
  </header>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
    // We hide links now so that we can render them later.
    hide($content['links']);
    hide($content['comments']);
    
    // Hide article image, but define in open graph instead.
    hide($content['field_image']);
    
    print render($content);
    ?>
  </div>

  <?php if ($content['links']): ?>
    <footer class="link-wrapper">
      <?php render($content['links']); ?>
    </footer>
  <?php endif; ?>

</article>
