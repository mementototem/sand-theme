﻿<?php
 
/**
 * Implements hook_preprocess_HOOK() for HTML document templates.
 * Adds body classes if certain regions have content.
 */
function sand_preprocess_html(&$variables) {
  if (!empty($variables['page']['sidebar'])) {
    $variables['classes_array'][] = 'have-sidebar';
  }
  else {
    $variables['classes_array'][] = 'no-sidebar';
  }
  
  // Add day as body class for colorize footer
  $day = date('l');
  $variables['classes_array'][] = strtolower($day);
  
  // Add external CSS
  drupal_add_css(
    'http://fonts.googleapis.com/css?family=Fondamento',
    array('type' => 'external')
  );
}

/**
 * Implements hook_preprocess_HOOK() for page templates.
 */
function sand_preprocess_page(&$variables) {
  // Pass the main menu and secondary menu to the template as render arrays.
  if (!empty($variables['main_menu'])) {
    $variables['main_menu']['#attributes']['id'] = 'main-menu-links';
    $variables['main_menu']['#attributes']['class'] = array('links', 'clearfix');
  }

  if (!empty($variables['secondary_menu'])) {
    $variables['secondary_menu']['#attributes']['id'] = 'secondary-menu-links';
    $variables['secondary_menu']['#attributes']['class'] = array('links', 'inline',
    'clearfix');
  }

  // Always print the site name and slogan, but if they are toggled off,
  // we'll just hide them visually.
  $variables['hide_site_name'] = theme_get_setting('features.name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('features.slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }

  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
}
 
/**
 * Implements hook_preprocess_HOOK() for maintenance page templates.
 */
function sand_preprocess_maintenance_page(&$variables) {
  // If database not active, remove default 'Drupal' as site name
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  
  $variables['styles'] = new RenderWrapper('drupal_get_css');
  // Manual add CSS file while database is not active
  $libraries = array(
    '#attached' => array(
      'library' => array(
        array('sand', 'maintenance_page'),
      ),
    ),
  );
  
  drupal_render($libraries);
  
  $site_config = \Drupal::config('system.site');
  // Always print the site name and slogan, but if they are toggled off,
  // we'll just hide them visually.
  $variables['hide_site_name'] = theme_get_setting('features.name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('features.slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    $variables['site_name'] = check_plain($site_config->get('name'));
  }
  if ($variables['hide_site_slogan']) {
    $variables['site_slogan'] = check_plain($site_config->get('slogan'));
  }
}

/**
 * Implements hook_library_info().
 */
function sand_library_info() {
  $path = drupal_get_path('theme', 'sand');
  $libraries['maintenance_page'] = array(
    'version' => \DRUPAL::VERSION,
    'css'     => array(
      $path . '/css/maintenance-page.css' => array (
        'group' => CSS_AGGREGATE_THEME,
      ),
    ),
  );
  
  return $libraries;
}

/**
 * Implements hook_preprocess_HOOK() for node templates.
 */
function sand_preprocess_node(&$variables) {
  // Remove the "Add new comment" link on teasers or when the comment form is
  // displayed on the page.
  if ($variables['teaser'] || !empty($variables['content']['comments']['comment_form'])) {
    unset($variables['content']['links']['comment']['#links']['comment-add']);
  }

  // Custom submitted message
  if ($variables['display_submitted']) {
    $variables['submitted'] = t('<span class="date">!date</span><span class="year">!year</span>',
    array(
      '!date' => format_date($variables['node']->created, 'custom', 'M d'),
      '!year' => format_date($variables['node']->created, 'custom', 'Y'),
    ));
  }
}

/**
 * Implements theme_field__field_type().
 */
function sand_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label either as a visible list or make it visually hidden for accessibility.
  $hidden_class = empty($variables['label_hidden']) ? '' : ' visually-hidden';
  $output .= '<h3 class="field-label' . $hidden_class . '">' . $variables['label'] . ': </h3>';

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] .'>' . $output . '</div>';

  return $output;
}